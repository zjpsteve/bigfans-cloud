package com.bigfans.paymentservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;


/**
 * 
 * @Description:支付实体
 * @author lichong 
 * 2014年12月14日下午4:36:58
 *
 */
@Data
@Table(name="Payment")
public class PaymentEntity extends AbstractModel {

	private static final long serialVersionUID = 6404772131152718534L;

	public static final String STATUS_PAY_SUCCESS = "SUCCESS";
	public static final String STATUS_PAY_FAILURE = "FAILURE";
	
	@Column(name="order_id")
	protected String orderId;
	@Column(name="buyer_id")
	protected String buyerId;
	@Column(name = "subject")
	protected String subject;
	@Column(name = "description")
	protected String description;
	@Column(name="pay_amount")
	protected BigDecimal payAmount;
	@Column(name="method_id")
	protected String methodId;
	@Column(name="status")
	protected String status;
	@Column(name="trade_no")
	protected String tradeNo;
	@Column(name="trade_type")
	protected String tradeType;

	public String getModule() {
		return "Payment";
	}

}