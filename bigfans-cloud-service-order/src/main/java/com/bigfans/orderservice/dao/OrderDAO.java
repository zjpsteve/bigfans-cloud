package com.bigfans.orderservice.dao;


import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.orderservice.model.Order;

import java.util.List;

/**
 * 
 * @Description:订单Mapper
 * @author lichong 
 * 2014年12月14日下午4:05:14
 *
 */
public interface OrderDAO extends BaseDAO<Order> {

	Order getByUser(String userId, String orderId) ;
	
	List<Order> listByUser(String userId, Long start, Long pagesize, boolean pageable) ;
	
	int updateStatus(String orderId, String status);
	
}
