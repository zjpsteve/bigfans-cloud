package com.bigfans.orderservice.exception;

import com.bigfans.framework.exception.ServiceRuntimeException;
import lombok.Data;

/**
 * 
 * @Description:库存不足
 * @author lichong
 * 2015年8月28日上午11:28:25
 *
 */
@Data
public class OrderStockOutException extends ServiceRuntimeException {

	private static final long serialVersionUID = -2954947819671607683L;

	private String orderId;

	public OrderStockOutException(String orderId) {
		this.orderId = orderId;
	}
	
}
