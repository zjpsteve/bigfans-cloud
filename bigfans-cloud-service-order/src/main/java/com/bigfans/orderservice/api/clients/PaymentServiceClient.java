package com.bigfans.orderservice.api.clients;

import com.bigfans.Constants;
import com.bigfans.framework.web.CookieHolder;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-03-20 下午8:53
 **/
@Component
public class PaymentServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<String> createPayment() {
        String userToken = CookieHolder.getValue(Constants.TOKEN.KEY_NAME);
        return CompletableFuture.supplyAsync(() -> {
            return null;
        });
    }

}
