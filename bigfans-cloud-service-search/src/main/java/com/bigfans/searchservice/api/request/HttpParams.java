package com.bigfans.searchservice.api.request;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2016年3月26日 上午10:33:22 
 * @version V1.0
 */
public class HttpParams {
	
	public interface SearchFilter {
		String KEYWORD = "q";
		String CATEGORY = "cat";
		String PRICE = "priceRange";
		String BRAND = "brand";
		String FILTER = "filters";
		String ORDER_BY = "orderby";
		String SORT = "sort";
		String CURRENT_PAGE = "cp";
	}

}
