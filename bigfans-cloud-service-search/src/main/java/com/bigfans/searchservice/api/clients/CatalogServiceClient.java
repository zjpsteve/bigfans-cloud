package com.bigfans.searchservice.api.clients;

import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.utils.CollectionUtils;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.searchservice.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class CatalogServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Category> getCategory(String catId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/categories/{catId}").build().expand(catId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse resp = responseEntity.getBody();
            Map data = (Map) resp.getData();
            Category category = BeanUtils.mapToModel(data, Category.class);
            return category;
        });
    }

    public CompletableFuture<Product> getProduct(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/product/{prodId}").build().expand(prodId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse resp = responseEntity.getBody();
            Map data = (Map) resp.getData();
            Product product = BeanUtils.mapToModel(data, Product.class);
            return product;
        });
    }

    public CompletableFuture<List<ProductAttribute>> getAttributesByProductId(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/attributes?prodId={prodId}").build().expand(prodId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse response = responseEntity.getBody();
            List data = (List) response.getData();
            List<ProductAttribute> attributes = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                Map attrMap = (Map) data.get(i);
                ProductAttribute attr = new ProductAttribute();
                attr.setOptionId((String) attrMap.get("option_id"));
                attr.setOptionName((String) attrMap.get("option_name"));
                attr.setValueId((String) attrMap.get("id"));
                attr.setValue((String) attrMap.get("value"));
                attributes.add(attr);
            }
            return attributes;
        });
    }

    public CompletableFuture<List<ProductSpec>> getSpecsByProductId(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/specs?prodId={prodId}").build().expand(prodId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse response = responseEntity.getBody();
            List data = (List) response.getData();
            List<ProductSpec> specs = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                Map m = (Map) data.get(i);
                ProductSpec spec = BeanUtils.mapToModel(m, ProductSpec.class);
                specs.add(spec);
            }
            return specs;
        });
    }

    public CompletableFuture<List<Tag>> getTagsByProductId(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/tags?prodId={prodId}").build().expand(prodId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse response = responseEntity.getBody();
            List data = (List) response.getData();
            List<Tag> tags = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                Map m = (Map) data.get(i);
                Tag t = BeanUtils.mapToModel(m, Tag.class);
                tags.add(t);
            }
            return tags;
        });
    }

    public CompletableFuture<Brand> getBrand(String brandId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/brands/{brandId}").build().expand(brandId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse resp = responseEntity.getBody();
            Map data = (Map) resp.getData();
            Brand brand = BeanUtils.mapToModel(data, Brand.class);
            return brand;
        });
    }

    public CompletableFuture<Tag> getTag(String tagId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/tags/{tagId}").build().expand(tagId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse resp = responseEntity.getBody();
            Map data = (Map) resp.getData();
            Tag tag = BeanUtils.mapToModel(data, Tag.class);
            return tag;
        });
    }

    public CompletableFuture<List<AttributeValue>> getAttributesByIdList(List<String> idList) {
        return CompletableFuture.supplyAsync(() -> {
            String ids = CollectionUtils.join(idList, ",");
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/attributes?ids={ids}").build().expand(ids).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse resp = responseEntity.getBody();
            List data = (List) resp.getData();
            List<AttributeValue> attributeValues = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                Map m = (Map) data.get(i);
                AttributeValue attributeValue = BeanUtils.mapToModel(m, AttributeValue.class);
                attributeValues.add(attributeValue);
            }
            return attributeValues;
        });
    }
}
