package com.bigfans.searchservice.api;

import com.bigfans.framework.es.request.SearchResult;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.searchservice.model.Product;
import com.bigfans.searchservice.service.RecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lichong
 * @create 2018-03-18 上午11:50
 **/
@RestController
public class RecommendApi extends BaseController {

    @Autowired
    private RecommendService recommendService;

    @GetMapping(value = "/mlt")
    public RestResponse getMoreLikeThis(@RequestParam(value = "prodId") String prodId){
        SearchResult<Product> searchResult = recommendService.moreLikeThis(prodId, 10);
        return RestResponse.ok(searchResult);
    }

}
