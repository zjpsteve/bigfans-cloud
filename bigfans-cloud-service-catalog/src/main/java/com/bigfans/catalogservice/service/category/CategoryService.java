package com.bigfans.catalogservice.service.category;

import com.bigfans.catalogservice.model.Category;
import com.bigfans.catalogservice.model.CategoryAttribute;
import com.bigfans.catalogservice.model.SpecOption;
import com.bigfans.framework.dao.BaseService;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 *
 */
public interface CategoryService extends BaseService<Category> {

	List<Category> listSubCats(String pid) throws Exception;
	
	List<Category> listSubCats(String pid, boolean recursive) throws Exception;
	
	List<Category> listByLevel(int level) throws Exception;

	List<Category> listByLevel(int level, boolean recursive) throws Exception;

	Category loadParents(String categoryId) throws Exception;
	
	/**
	 * 获取当前类别的所有父类别ID
	 * @param categoryId
	 * @return
	 * @throws Exception
	 */
	List<String> listParentIds(String categoryId) throws Exception;
	
	List<Category> listWithHierarchy() throws Exception;
	
	List<Category> getNavigatorTree() throws Exception;

	void create(Category example, List<CategoryAttribute> calist, List<SpecOption> specList) throws Exception;
	
}
