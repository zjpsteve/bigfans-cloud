package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.Tag;
import com.bigfans.catalogservice.service.tag.TagService;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TagApi {
	
	@Autowired
	private TagService tagService;

	@GetMapping(value = "/tags")
	public RestResponse list(
			@RequestParam(value = "prodId") String prodId
	) throws Exception{
		assert (prodId != null);
		List<Tag> tags = tagService.listByProdId(prodId);
		return RestResponse.ok(tags);
	}

	@GetMapping(value = "/tags/{tagId}")
	public RestResponse getById(@PathVariable(value = "tagId") String tagId) throws Exception {
		Tag tag = tagService.load(tagId);
		return RestResponse.ok(tag);
	}

}
