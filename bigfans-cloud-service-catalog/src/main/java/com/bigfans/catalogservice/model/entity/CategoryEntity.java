package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 商品类别
 * 
 * @author lichong
 *
 */
@Data
@Table(name="Category")
public class CategoryEntity extends AbstractModel {

	public static final int LEVEL_TOP = 1;
	public static final int LEVEL_CAT_NAV = 2;
	
	private static final long serialVersionUID = -1260542803769812886L;

	@Column(name="name")
	protected String name;
	@Column(name="parent_id")
	protected String parentId;
	@Column(name="image_path")
	protected String imagePath;
	@Column(name="level")
	protected Integer level;
	@Column(name="order_num")
	protected Integer orderNum;
	@Column(name="show_innav")
	protected Boolean showInNav;
	@Column(name="show_inhome")
	protected Boolean showInHome;
	@Column(name="description")
	protected String description;
	
	public String getModule() {
		return "Category";
	}

	@Override
	public String toString() {
		return "ProductCategory [name=" + name + ", parentId=" + parentId + "]";
	}


}
