package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;


/**
 * 
 * @Title: 
 * @Description: 商品规格值:挂在商品下面
 * @author lichong 
 * @date 2015年9月21日 上午11:25:00 
 * @version V1.0
 */
@Data
@Table(name="SpecValue")
public class SpecValueEntity extends AbstractModel {

	public String getModule() {
		return "SpecValue";
	}
	
	private static final long serialVersionUID = -3760056666983029910L;

	@Column(name="value")
	protected String value;
	@Column(name="option_id")
	protected String optionId;
	@Column(name="image_path")
	protected String imagePath;
	@Column(name="category_id")
	protected String categoryId;
	
}
