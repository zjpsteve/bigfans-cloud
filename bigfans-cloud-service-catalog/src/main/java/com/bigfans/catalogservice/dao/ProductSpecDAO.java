package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.ProductSpec;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

/**
 * 
 * @Title: 
 * @Description: 商品和规格关联表DAO
 * @author lichong 
 * @date 2015年12月21日 下午9:55:11 
 * @version V1.0
 */
public interface ProductSpecDAO extends BaseDAO<ProductSpec> {

	ProductSpec getProductSpec(String pgId , String optionId , String valueId);
	
	List<ProductSpec> listByProdId(String prodId);
	
}
