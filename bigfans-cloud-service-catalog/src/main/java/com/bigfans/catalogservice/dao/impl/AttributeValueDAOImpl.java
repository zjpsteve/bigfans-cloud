package com.bigfans.catalogservice.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bigfans.catalogservice.dao.AttributeValueDAO;
import com.bigfans.catalogservice.model.AttributeValue;
import com.bigfans.framework.dao.MybatisDAOImpl;
import org.springframework.stereotype.Repository;

/**
 * 
 * @Title: 
 * @Description: 属性值DAO操作
 * @author lichong 
 * @date 2015年12月21日 上午11:14:57 
 * @version V1.0
 */
@Repository(AttributeValueDAOImpl.BEAN_NAME)
public class AttributeValueDAOImpl extends MybatisDAOImpl<AttributeValue> implements AttributeValueDAO {
	
	public static final String BEAN_NAME = "attributeValueDAO";
	
	@Override
	public List<AttributeValue> listByProduct(String productId) {
		Map<String , Object> params = new HashMap<String , Object>();
		params.put("prodId", productId);
		return getSqlSession().selectList(className + ".listByProduct", params);
	}

	@Override
	public List<AttributeValue> listByProductGroup(String pgId) {
		Map<String , Object> params = new HashMap<String , Object>();
		params.put("pgId", pgId);
		return getSqlSession().selectList(className + ".listByProductGroup", params);
	}

	@Override
	public List<AttributeValue> listById(List<String> idList) {
		Map<String , Object> params = new HashMap<String , Object>();
		params.put("idList", idList);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<AttributeValue> listByAttribute(String optionId) {
		Map<String , Object> params = new HashMap<String , Object>();
		params.put("optionId", optionId);
		return getSqlSession().selectList(className + ".list", params);
	}

}
