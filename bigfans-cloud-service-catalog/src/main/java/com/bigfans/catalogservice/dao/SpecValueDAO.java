package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.SpecValue;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月31日下午9:11:57
 *
 */
public interface SpecValueDAO extends BaseDAO<SpecValue>{
	
	List<SpecValue> listById(String[] idList);
	
	List<SpecValue> listByOptionId(String optionId);

}
