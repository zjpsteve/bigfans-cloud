package com.bigfans.framework.kafka;

import java.lang.annotation.*;

/**
 * @author lichong
 * @create 2018-01-31 下午9:00
 **/
@Inherited
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface KafkaListener {

    String[] topics() default {};

    String id() default "";

    /* 处理这个listener消息时开启的线程数 */
    String threadCount() default "";

}
